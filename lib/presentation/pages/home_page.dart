import 'package:bloc_practical_work/presentation/bloc/size_bloc.dart';
import 'package:bloc_practical_work/presentation/bloc/size_event.dart';
import 'package:bloc_practical_work/presentation/bloc/size_state.dart';
import 'package:bloc_practical_work/presentation/theme/app_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "AnimatedContainer",
          style: AppFonts.s16W700.copyWith(color: Colors.white),
        ),
        backgroundColor: Colors.redAccent,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // double randWidth = SizeRepository();
          // double randHeight = Random().nextDouble() * 100;
          BlocProvider.of<SizeBloc>(context).add(RandomEvent());
        },
        child: const Icon(
          Icons.refresh,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 100),
          BlocBuilder<SizeBloc, SizeState>(
            builder: (context, state) {
              if (state is SizeSuccess) {
                return Center(
                  child: AnimatedContainer(
                    duration: const Duration(seconds: 1),
                    width: state.width,
                    height: state.height,
                    color: Color.fromRGBO(state.r, state.g, state.b, 1),
                  ),
                );
              }
              if (state is SizeError) {
                return Text(
                  state.errText,
                  style: AppFonts.s14W400,
                );
              }
              return const SizedBox();
            },
          ),
          // BlocListener<SizeBloc, SizeState>(
          //   listener: (context, state) {
          //     if (state is SizeSuccess) {
          //       showDialog(
          //         context: context,
          //         builder: (context) => AlertDialog.adaptive(
          //           content: Column(
          //             children: [
          //               const SizedBox(height: 20),
          //               Center(
          //                 child: AnimatedContainer(
          //                   duration: const Duration(seconds: 1),
          //                   width: state.width,
          //                   height: state.height,
          //                   color: Color.fromRGBO(
          //                     state.r,
          //                     state.g,
          //                     state.b,
          //                     1,
          //                   ),
          //                 ),
          //               ),
          //               const SizedBox(height: 20),
          //               IconButton(
          //                 onPressed: () {
          //                   Navigator.pop(context);
          //                   BlocProvider.of<SizeBloc>(context)
          //                       .add(RandomEvent());
          //                 },
          //                 icon: const Icon(
          //                   Icons.refresh,
          //                 ),
          //               )
          //             ],
          //           ),
          //         ),
          //       );
          //     }
          //   },
          //   child: const Center(child: Text("пусто")),
          // ),
        ],
      ),
    );
  }
}
