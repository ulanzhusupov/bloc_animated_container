abstract class SizeState {}

class SizeInitial extends SizeState {}

class SizeSuccess extends SizeState {
  final double width;
  final double height;
  final int r;
  final int g;
  final int b;
  SizeSuccess({
    required this.width,
    required this.height,
    required this.r,
    required this.g,
    required this.b,
  });
}

class SizeLoading extends SizeState {}

class SizeError extends SizeState {
  final String errText;

  SizeError({required this.errText});
}
