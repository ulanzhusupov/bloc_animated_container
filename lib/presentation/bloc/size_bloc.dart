import 'dart:math';

import 'package:bloc_practical_work/presentation/bloc/size_event.dart';
import 'package:bloc_practical_work/presentation/bloc/size_state.dart';
import 'package:bloc_practical_work/repository/size_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SizeBloc extends Bloc<SizeEvent, SizeState> {
  final SizeRepository repository;

  SizeBloc({required this.repository}) : super(SizeInitial()) {
    on<RandomEvent>((event, emit) {
      emit(SizeLoading());
      try {
        emit(
          SizeSuccess(
            width: repository.getRandom(300).toDouble(),
            height: repository.getRandom(300).toDouble(),
            r: repository.getRandomColor(256),
            g: repository.getRandomColor(256),
            b: repository.getRandomColor(256),
          ),
        );
      } catch (e) {
        emit(SizeError(errText: e.toString()));
      }
    });
  }
}
