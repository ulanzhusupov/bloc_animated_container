import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s14W400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );
  static const TextStyle s16W700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
  );
}
