import 'dart:math';

class SizeRepository {
  int getRandom(int num) {
    return Random().nextInt(num) + 50;
  }

  int getRandomColor(int num) {
    return Random().nextInt(num);
  }
}
