import 'package:bloc_practical_work/presentation/bloc/size_bloc.dart';
import 'package:bloc_practical_work/presentation/pages/home_page.dart';
import 'package:bloc_practical_work/repository/size_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => SizeRepository(),
      child: BlocProvider(
        create: (context) => SizeBloc(
          repository: RepositoryProvider.of<SizeRepository>(context),
        ),
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: const HomePage(),
        ),
      ),
    );
  }
}
